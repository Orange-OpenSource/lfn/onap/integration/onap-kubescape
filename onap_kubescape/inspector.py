"""
Post process kubescape tests performed on ONAP cluster.
"""

__title__ = "onap_kubescape"
__summary__ = (
    "Module for verifying versions of CPython and OpenJDK binaries installed"
    " in the kubernetes cluster containers."
)
__version__ = "0.1.0"
__author__ = "morgan.richomme@orange.com"
__license__ = "Apache-2.0"
__copyright__ = "Copyright 2021 Orange."

import logging
import os
import subprocess
import sys
import time
import yaml
from typing import List, Optional
from junitparser import TestCase, Attr, JUnitXml
from xtesting.core import testcase  # pylint: disable=import-error
from jinja2 import (  # pylint: disable=import-error
    Environment,
    select_autoescape,
    PackageLoader,
)


# Loggerimport pathlib
logging.basicConfig()
LOGGER = logging.getLogger("onap-versions-status-inspector")
LOGGER.setLevel("DEBUG")
DEFAULT_NAMESPACE = "onap"
DEFAULT_TEST_NAME = "onap_kubescape"
REPORTING_FILE = "/tmp/onap_kubescape.html"
DEFAULT_KUBESCAPE_SCAN = "" #  all but could be nsap, mitre
KUBESCAPE_ONAP_OPTION = "--submit"
CRITERIA_FILE = os.environ.get(
    "KUBESCAPE_CRITERIA_FILE_PATH",
    "/tmp/onap_kubescape/config.yaml")
JUNIT_RESULT_FILE = f"/tmp/{DEFAULT_TEST_NAME}"

PTABLE_RESULT_FILE = f"/tmp/{DEFAULT_TEST_NAME}.txt"
JUNIT_HTML_PAGE = f"/tmp/{DEFAULT_TEST_NAME}.html"
KUBESCAPE_JUNIT_CMD = (f"kubescape scan {DEFAULT_KUBESCAPE_SCAN}\
    {KUBESCAPE_ONAP_OPTION} --include-namespaces {DEFAULT_NAMESPACE}\
    -f junit\
    -o {JUNIT_RESULT_FILE}")
KUBESCAPE_PTABLE_CMD = (f"kubescape scan {DEFAULT_KUBESCAPE_SCAN}\
    {KUBESCAPE_ONAP_OPTION} --include-namespaces {DEFAULT_NAMESPACE}\
    -f pretty-table\
    -o {PTABLE_RESULT_FILE}")


class MyTestCase(TestCase):
    """Attributes of the junit test to evaluate test criteria (PASS/FAIL)"""
    name = Attr()
    resources = Attr()
    excluded = Attr()
    filed = Attr()


def execute_command(cmd):
    """Execute shell command."""
    proc = subprocess.Popen(cmd,
               stdout = subprocess.PIPE,
               stderr = subprocess.PIPE,
               shell = True,
    )
    stdout, stderr = proc.communicate()
    return proc.returncode, stdout, stderr

def generate_junit_result(junit_file, junit_html_page):
    """Generate html built-in page from junit"""
    stream = os.popen(f"junit2html {junit_file} {junit_html_page}")
    output = stream.read()
    LOGGER.debug(f"Junit html page generated in {junit_html_page}")

def get_target_value(case_name):
    with open(CRITERIA_FILE) as file:
        doc = yaml.load(file, Loader=yaml.FullLoader)
        try:
            return doc[case_name]
        except KeyError:
            return 0

def parse_result_file(junit_file):
    """From junit file retrieve the success rate per case."""
    results = {}
    LOGGER.debug("Parse junit file")

    for suite in JUnitXml.fromfile(junit_file):
        # handle suites
        for case in suite:
            my_case = MyTestCase.fromelem(case)
            # print(my_case.name)
            risk_rate = 0
            if int(my_case.resources) > 0:
                risk_rate = round(int(my_case.filed) / int(my_case.resources) * 100, 1)
            testcase_res = { 'resources': my_case.resources,
                             'excluded': my_case.excluded,
                             'filed': my_case.filed,
                             'risk_rate': risk_rate,
                             'target': get_target_value(case.name)}
            # TODO wrong criteria
            if risk_rate != 'skipped':
                results[my_case.name] = testcase_res
    return results

def get_waivers(waiver_file_path):
    """Get waivers..."""
    LOGGER.debug("Get waiver file")

def evaluate_test_criteria(results):
    """Set Testcase criteria."""
    # Retrieve success rate per category and compare to expectation
    # if one criteria is failed => test is failed
    test_criteria = testcase.TestCase.EX_OK
    for test_name in results:
        # get success rate
        test_risk_rate = results[test_name]['risk_rate']
        test_target = get_target(test_name)
        LOGGER.debug(f"Risk rate of {test_name}: {test_risk_rate}%")
        LOGGER.debug(f"Target: {test_target}%")
        if test_risk_rate > test_target:
            LOGGER.info(f"Test {test_name} FAIL")
            test_criteria = testcase.TestCase.EX_TESTCASE_FAILED
        else:
            LOGGER.info(f"{test_name} PASS")
    return test_criteria

def get_target(test_name):
    """Retrieve test success rate target from config."""
    with open(CRITERIA_FILE) as fh:
        read_data = yaml.load(fh, Loader=yaml.FullLoader)
        # Print YAML data before sorting
        #if (test_name in read_data):
    try:
        for key, value in read_data.items():
            if key == test_name:
                return int(value['criteria'])
    except TypeError:
        LOGGER.warning(f"Impossible to retrieve {test_name} test criteria")
    return 0

def generate_reporting(risk_res, data):
    """generate ONAP reporting"""
    LOGGER.debug("Generate reporting")
    # Create reporting page
    jinja_env = Environment(
        autoescape=select_autoescape(["html"]),
        loader=PackageLoader('onap_kubescape'),
    )
    page_info = {"title": "ONAP Kubescape reporting",
                "risk_rate": risk_res,
                }
    jinja_env.get_template("kubescape.html.j2").stream(
        info=page_info, data=data
    ).dump("{}".format(REPORTING_FILE))

class Inspector(testcase.TestCase):
    """Inspector CLass."""

    def __init__(self, **kwargs):
        """Init the testcase."""
        if "case_name" not in kwargs:
            kwargs["case_name"] = "onap_kubescape"
        super().__init__(**kwargs)
        self.start_time = None
        self.stop_time = None

    def run(self):
        """Execute the version Inspector."""
        # First try in junit format to build the html raw reporting
        self.start_time = time.time()
        LOGGER.info("Execute kubescape (junit format).")
        execute_command(KUBESCAPE_JUNIT_CMD)
        # LOGGER.info("Generate junit raw html reporting page.")
        # generate_junit_result(f"{JUNIT_RESULT_FILE}.xml", JUNIT_HTML_PAGE)
        LOGGER.info("Replay to get pretty table format.")
        execute_command(KUBESCAPE_PTABLE_CMD)

        self.stop_time = time.time()
        # parsing
        parsed_results = parse_result_file(f"{JUNIT_RESULT_FILE}.xml")
        LOGGER.debug(parsed_results)
        test_criteria = evaluate_test_criteria(parsed_results)
        LOGGER.info(f'Test final result: {test_criteria}')
        generate_reporting(test_criteria, parsed_results)
        return test_criteria


def main(argv: Optional[List[str]] = None):
    """Main entrypoint of the module for formating
    kubescape results
    """
    test = Inspector()
    test.run()

if __name__ == "__main__":
    sys.exit(main())
